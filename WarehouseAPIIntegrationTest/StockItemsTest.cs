using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace WarehouseAPIIntegrationTest
{
    public class Tests
    {
        private WebApplicationFactory<WarehouseAPI.Startup> _factory;

        [OneTimeSetUp]
        public void SetupFactory()
        {
            _factory = new WebApplicationFactory<WarehouseAPI.Startup>().WithWebHostBuilder(
                builder => builder.UseStartup<WarehouseAPI.Startup>().UseEnvironment("IntegrationTest")
            );
        }

        [OneTimeTearDown]
        public void TearDownFactory()
        {
            _factory.Dispose();
        }

        [Test]
        public async Task StockItemListForSupplier1()
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var result = await client.GetAsync("/suppliers/1/stockitems?pageSize=3&pageToken=1");
            var content = await result.Content.ReadAsStringAsync();

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, result.StatusCode);
            Assert.AreEqual(
                "{\"stockItems\":[" +
                    "{\"stockItemID\":223,\"stockItemName\":\"Chocolate echidnas 250g\",\"supplierID\":1,\"colorID\":null,\"colorName\":null,\"brand\":null,\"barcode\":\"8792838293728\"}," +
                    "{\"stockItemID\":224,\"stockItemName\":\"Chocolate frogs 250g\",\"supplierID\":1,\"colorID\":null,\"colorName\":null,\"brand\":null,\"barcode\":\"8792838293987\"}," +
                    "{\"stockItemID\":225,\"stockItemName\":\"Chocolate sharks 250g\",\"supplierID\":1,\"colorID\":null,\"colorName\":null,\"brand\":null,\"barcode\":\"8792838293234\"}" +
                "]," +
                $"\"nextPage\":\"{client.BaseAddress}suppliers/1/stockitems/suppliers/1/stockitems?pageToken=2&pageSize=3\"}}",
                content
            );
        }
    }
}